This repository is an exercise to practise advanced git topics.
It is part of a series of exercices.

Read the exercice statement in STATEMENT.txt, and use git commands to get the expected result.
If having difficulties solving the exercise, read SOLUTION.txt.
